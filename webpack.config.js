var webpack = require("webpack");
var path = require("path");
 
var DEV = path.resolve(__dirname, "src");
var OUTPUT = path.resolve(__dirname, "public/dist/js");
 
var config = {
  entry: DEV + "/main.js",
  output: {
    path: OUTPUT,
    filename: "app.js"
  },
  resolve: {
    extensions: ['', '.js', '.jsx'],
  },
  externals: {
    'cheerio': 'window',
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true,
  },
  module: {
    loaders: [{
        include: DEV,
        exclude: /(node_modules|bower_components)/,
        loader: "babel",
        query: {
          presets: ['react', 'es2015'],
        }
    }]
  }
};
 
module.exports = config;