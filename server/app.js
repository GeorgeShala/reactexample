const fs = require('fs')
const express = require('express')
var history= require('connect-history-api-fallback')

global.app = express()

const PORT = 8080

/////////////////

/////////////////

app.set('views', __dirname + '/views')

app.set('views', '../public/')

app.use(express.static('../public'))


app.get('*', function(req,res) {
	res.status(404)
	res.end('page not found')
})

app.listen(PORT, function () {
  console.log(`Example app listening on port ${PORT}!`)
})